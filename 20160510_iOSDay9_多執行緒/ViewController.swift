//
//  ViewController.swift
//  20160510_iOSDay9_多執行緒
//
//  Created by ChenSean on 5/10/16.
//  Copyright © 2016 ChenSean. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
    
    func ticker(timer: NSTimer) {
        // 在 swift 中，宣告 local static 變數方法
        struct _tmp {
            static var n = 0
        }
        
        if _tmp.n == 10 {
            // 10 秒後 timer 停掉
            timer.invalidate()
        }
        
        _tmp.n += 1
        
        NSLog("測試 Timer: " + String(_tmp.n))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(ticker(_:)), userInfo: nil, repeats: true)
        
        
        // 抓到佇列的進入點
        let q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        
        print("h1")
        
        // asyn 是將佇列中的執行緒拿出來
        dispatch_async(q) { 
            for i in 0 ..< 10 {
                
                // 設定信號，管理只有一個 flag 進入，Teacher: 東部火車，或在兩站中間，交遞一個記號符號，才確保只有一條鐵軌上只有一台火車行駛。
                let signal = dispatch_semaphore_create(0)
                
                sleep(2)
                dispatch_async(dispatch_get_main_queue(), {
                    self.myLabel.text = String(i)
                })
                
                if i == 5 {
                    let alert = UIAlertController(title: "警告", message: nil, preferredStyle: .Alert)
                    let ok = UIAlertAction(title: "ok", style: .Default, handler: { (action) in
                        dispatch_semaphore_signal(signal)
                        self.dismissViewControllerAnimated(true, completion: nil)
                    })
                    alert.addAction(ok)
                    self.presentViewController(alert, animated: true, completion: nil)
                } else {
                    // 釋放 signal
                    dispatch_semaphore_signal(signal)
                }
                
                dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER)
                // google 關鍵字：作業系統，信號 (single critical section)
                
                // NSRunLoop -> 有些 code 會用這個去做 timer 設定，
                // 這個東西是 iOS 最大的迴圈，會一直去捕捉 app 的訊號，window 也有類似的機制，捕捉 mouse 等執行的訊號，
                // 但是他太大了，很底層，不好處理，不建議直接使用。
                
                NSLog("%d", i)
            }
        }
        
        dispatch_sync(q){
            for i in 10 ..< 20{
                NSLog("%dThread2", i)
            }
        }
        
        print("hello")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

